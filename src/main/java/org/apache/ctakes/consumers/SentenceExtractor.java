/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.ctakes.consumers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.ctakes.typesystem.type.textspan.Sentence;
import org.apache.ctakes.utils.Utils;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.util.ViewUriUtil;
import org.apache.uima.fit.descriptor.ConfigurationParameter;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

/**
 * Read cTAKES annotations from XMI files.
 *  
 * @author dmitriy dligach
 */
public class SentenceExtractor {
  
  static interface Options {

    @Option(
        longName = "xmi-dir",
        description = "path to xmi files")
    public File getInputDirectory();

      @Option(
              longName = "output-dir",
              description = "path to sentence-split files")
      public String getOutputDir();
  }
  
	public static void main(String[] args) throws Exception {
		  
		Options options = CliFactory.parseArguments(Options.class, args);
    CollectionReader collectionReader = Utils.getCollectionReader(options.getInputDirectory());
    AnalysisEngine annotationConsumer = AnalysisEngineFactory.createEngine(RelationContextPrinter.class, "OutputDir", options.getOutputDir());
		SimplePipeline.runPipeline(collectionReader, annotationConsumer);
	}

  /**
   * Print events and entities.
   *
   * @author dmitriy dligach
   */
  public static class RelationContextPrinter extends JCasAnnotator_ImplBase {

    @ConfigurationParameter(
        name = "OutputDir",
        mandatory = true,
        description = "path to store restructured files")
    private String outputDir;

    @Override
    public void process(JCas jCas) throws AnalysisEngineProcessException {
      List<String> sentences = new ArrayList<>();
      for(Sentence sentence : JCasUtil.select(jCas, Sentence.class)) {
        String withLineBreaks = sentence.getCoveredText();
        //String noLineBreaks = withLineBreaks.replace("\n", " ");
        //System.out.println(" * " + noLineBreaks);
        //System.out.println();
        sentences.add(withLineBreaks);
      }

      File noteFile = new File(ViewUriUtil.getURI(jCas).toString());
      String fileName = noteFile.getName();
      String outString = String.join("\n", sentences);
      try {
          Files.write(Paths.get(outputDir + fileName), outString.getBytes());
      } catch (IOException e) {
          e.printStackTrace();
      }
    }
  }
}

  
